<?php  namespace Aedart\Model\Filename\Traits; 

use Aedart\Model\Filename\Exceptions\InvalidFilenameException;

/**
 * Trait Filename
 *
 * @see \Aedart\Model\Filename\Interfaces\FilenameAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Filename\Traits
 */
trait FilenameTrait {

    /**
     * Filename
     *
     * @var string|null
     */
    protected $filename = null;

    /**
     * Set the filename
     *
     * @param string $name Filename
     *
     * @return void
     *
     * @throws InvalidFilenameException If given filename is invalid
     */
    public function setFilename($name){}

    /**
     * Get the filename
     *
     * If no filename has been set, this method sets and
     * returns a default filename, if any is available
     *
     * @return string|null The filename or null if none has been set
     */
    public function getFilename(){}

    /**
     * Get a default filename, if any is available
     *
     * @return string|null A default filename or null if none is available
     */
    public function getDefaultFilename(){}

    /**
     * Check if a filename has been set
     *
     * @return bool True if a filename has been set, false if not
     */
    public function hasFilename(){}

    /**
     * Check if a default filename is available
     *
     * @return bool True if a default filename is available, false if not
     */
    public function hasDefaultFilename(){}

    /**
     * Check if the given filename is valid; e.g. contains chars it is not supposed,
     * incorrect or disallowed file-extension, etc
     *
     * @param mixed $name Filename to be validated
     *
     * @return bool True if the given filename is valid, false if not
     */
    public function isFilenameValid($name){}

}