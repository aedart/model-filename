<?php  namespace Aedart\Model\Filename\Interfaces;
use Aedart\Model\Filename\Exceptions\InvalidFilenameException;

/**
 * Interface Filename Aware
 *
 * Components that implement this, promise that a filename can be specified and retrieve,
 * when it is needed. In addition, a default filename might be available, if none has
 * been set prior to obtaining it (implementation dependent).
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Filename\Interfaces
 */
interface FilenameAware {

    /**
     * Set the filename
     *
     * @param string $name Filename
     *
     * @return void
     *
     * @throws InvalidFilenameException If given filename is invalid
     */
    public function setFilename($name);

    /**
     * Get the filename
     *
     * If no filename has been set, this method sets and
     * returns a default filename, if any is available
     *
     * @return string|null The filename or null if none has been set
     */
    public function getFilename();

    /**
     * Get a default filename, if any is available
     *
     * @return string|null A default filename or null if none is available
     */
    public function getDefaultFilename();

    /**
     * Check if a filename has been set
     *
     * @return bool True if a filename has been set, false if not
     */
    public function hasFilename();

    /**
     * Check if a default filename is available
     *
     * @return bool True if a default filename is available, false if not
     */
    public function hasDefaultFilename();

    /**
     * Check if the given filename is valid; e.g. contains chars it is not supposed,
     * incorrect or disallowed file-extension, etc
     *
     * @param mixed $name Filename to be validated
     *
     * @return bool True if the given filename is valid, false if not
     */
    public function isFilenameValid($name);
}