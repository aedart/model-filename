<?php  namespace Aedart\Model\Filename\Exceptions; 

/**
 * Class Invalid Filename Exception
 *
 * Throw this exception when an invalid filename has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Filename\Exceptions
 */
class InvalidFilenameException extends \InvalidArgumentException{

}